import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

import Snackbar from '../../components/Snackbar/Snackbar';

import services from '../../services/index';

const useStyles = makeStyles(theme => ({
  container: {
    width: '80%',
    textAlign: 'center'
  },
  title: {
    textAlign: 'center'
  },
  border: {
    border: '2px solid black'
  },
  buttonWrapper: {
    position: 'relative'
  },
  buttonLoader: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1)
  },
  button: {
    margin: theme.spacing(1)
  },
  paper: {
    padding: theme.spacing(3, 2)
  }
}));

export default function WhitelistOne() {
  const classes = useStyles();

  const [formValue, setFormValue] = useState({
    merchantMsisdn: 0,
    supplierId: 0
  });
  const [test, setTest] = useState('aa');

  //snackbar
  const [open, setOpen] = useState(false);
  const [error, setError] = useState(false);
  const [snackbarText, setSnackbarText] = useState('');

  //loading
  const [loading, setLoading] = useState(false);
  const [whitelistLoading, setWhitelistLoading] = useState(false);

  function submit() {
    setOpen(false);
    setError(false);
    setWhitelistLoading(true)
    services
      .whitelist(formValue.supplierId, formValue.merchantMsisdn)
      .then(result => {
        setFormValue({
          merchantMsisdn: 0,
          supplierId: 0
        });
        setSnackbarText('Whitelist Success');
        setOpen(true);
        setWhitelistLoading(false)
      })
      .catch(err => {
        setFormValue({
          merchantMsisdn: 0,
          supplierId: 0
        });
        setSnackbarText('Internal Server Error');
        setError(true);
        setOpen(true);
        setWhitelistLoading(false)
      });
  }

  const handleChange = name => event => {
    setFormValue({ ...formValue, [name]: event.target.value });
  };

  return (
    <Grid container direction="row" justify="center" alignItems="center">
      <Snackbar
        open={open}
        setOpen={setOpen}
        error={error}
        msg={snackbarText}
        setError={setError}
      ></Snackbar>
      <Grid container direction="row" alignItems="center">
        <Paper className={classes.root}>
          <Grid container direction="row">
            <TextField
              id="outlined-name"
              label="id supplier"
              value={formValue.supplierId}
              className={classes.textField}
              onChange={handleChange('supplierId')}
              margin="normal"
              variant="outlined"
            />
            <TextField
              id="outlined-number"
              label="msisdn"
              value={formValue.merchantMsisdn}
              type="number"
              className={classes.textField}
              onChange={handleChange('merchantMsisdn')}
              margin="normal"
              variant="outlined"
            />
          </Grid>
          <Grid container direction="row">
            <Button
              variant="contained"
              color="primary"
              className={classes.button}
              onClick={submit}
            >
              {whitelistLoading ? 'Loading' : 'Save' }
            </Button>
          </Grid>
        </Paper>
      </Grid>
    </Grid>
  );
}
