import React, { useState } from 'react';
import { TextField, InputAdornment, Button, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { CloudUpload } from '@material-ui/icons';
import services from '../../services/index';
import swal from 'sweetalert';
import FileDownload from 'js-file-download';

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1)
  }
}));

function WhitelistExcel() {
  const [files, setFiles] = useState({});

  const selectUpload = (e, index) => {
    const file = e.target.files;
    if (file.length) {
      if (file[0].type.includes('sheet')) {
        const fileReader = new FileReader();
        fileReader.readAsDataURL(file[0]);
        fileReader.onload = () => {
          setFiles({ ...files, [index]: file[0] });
        };
      } else {
        swal('Please Input Whitelist CSV', '', 'warning');
      }
    }
  };

  const handleUpload = async (action, index) => {
    if (files[index]) {
      try {
        const dataForm = new FormData();
        dataForm.append('file', files[index]);
        if (action === 'CREATE') {
          await services.uploadWhitelistCsvData(dataForm);
            swal(
              'Whitelist Success',
              'Harap tunggu 5 - 10 Menit karena data sedang diproses di sistem'
            );
        } else {
          const response = await services.uploadCheckWhitelistCsv(dataForm);
            FileDownload(response, 'response.xlsx');
            swal(
              'Whitelist Success',
              'mantap'
            );
        }
        
      } catch (e) {
        swal('Error', 'Something wrong with image upload', 'warning');
      }
    }
  };

  const classes = useStyles();

  return (
    <div>
      <Grid container direction="row" alignItems="center">
        <TextField
          id="ktm1"
          label="Foto Kartu Mahasiswa"
          type="file"
          variant="outlined"
          onChange={e => selectUpload(e, 1)}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <CloudUpload />
              </InputAdornment>
            )
          }}
        />
        <Button
          onClick={() => handleUpload('CREATE', 1)}
          variant="contained"
          className={classes.button}
        >
          Upload
        </Button>
        <Button
          onClick={() => handleUpload('CHECK', 1)}
          variant="contained"
          className={classes.button}
        >
          Check
        </Button>
      </Grid>
    </div>
  );
}

export default WhitelistExcel;
