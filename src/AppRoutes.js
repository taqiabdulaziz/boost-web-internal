import loadable from '@loadable/component';
import WhitelistOne from './components/Whitelist/WhitelistOne';
import WhitelistExcel from './components/Whitelist/WhitelistExcel';

const Dashboard = loadable(() => import('./containers/Dashboard/Dashboard'))
const Login = loadable(() => import('./containers/Login/Login'));


const routes = [
  {
    id: 'dashboard',
    icon: 'dashboard',
    title: 'home',
    path: '/dashboard',
    component: Dashboard,
    childs: [{
      id: 'WhitelistOne',
      icon: 'dashboard',
      title: 'Whitelist',
      path: 'whitelist',
      component: WhitelistOne,
    },{
      id: 'WhitelistExcel',
      icon: 'dashboard',
      title: 'Whitelist by Excel',
      path: 'whitelist-excel',
      component: WhitelistExcel
    }]
  },
  {
    id: 'login',
    icon: 'login',
    title: 'login',
    path: '/login',
    component: Login
  }
];

export default routes;