import axios from '../utils/axios';

function getOptions (content='json', qs) {

  let options = {
    headers: {},
  };

  if (content === 'json') {
    options.headers['Content-Type'] = 'application/json';
  } else {
    options.headers['Content-Type'] = content;
    options['responseType'] = 'arraybuffer';
  }

  if (localStorage.token) {
    options.headers.token = localStorage.token;
  }

  return options;
}

export default {
  whitelist(supplierId, merchantMsisdn) {
    return axios.post('v2/internal/whitelist-merchant', {
      supplierId, merchantMsisdn,
    });
  },
  uploadWhitelistCsvData(data) {
    return axios.post('v2/internal/whitelist-merchant/upload',
    data,
    getOptions('multipart/form-data'),
    )
  },
  uploadCheckWhitelistCsv(data) {
    return axios.post('v2/internal/whitelist-merchant/fetch/upload',
    data,
    getOptions('multipart/form-data'),
    )
  },
}
