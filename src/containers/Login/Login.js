import React, { useEffect } from 'react'
import { connect } from "react-redux"
import { login } from "../../store/actions/userAction"


function Login(props) {

  // useEffect(() => {
  //   console.log(props)
  //   props.login();
  // })

  useEffect(() => {
    console.log(props.user)
  }, [props])

  return (
    <div>
      <h1>LOGIN</h1>
      <button onClick={() => props.login()}>Login</button>
      
    </div>
  )
}

const mapStateToProps = state => {
  return {
    ...state
  }
}

const mapDispatchToProps = {
  login
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);