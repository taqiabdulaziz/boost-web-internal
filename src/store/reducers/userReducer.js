const initialState = {
  token: "token123"
}

export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case "LOGIN":
      return {
        ...state,
        token: action.payload
      }
  
    default:
      return state
  }
}