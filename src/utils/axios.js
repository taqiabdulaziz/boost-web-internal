/* eslint-disable no-undef */
import axios from 'axios';
// require('dotenv').config()
axios.defaults.baseURL = process.env.REACT_APP_SVC_SUPPLIER;

axios.interceptors.response.use(
  function(response) {
    return response.data;
  },
  function(error) {
    return Promise.reject(error.response.data);
    // return error;
  }
);

export default axios;

// export default function api() {
//   const headers = {
//     "Content-Type": "application/json"
//   };

//   const token = localStorage.token;

//   if (token) {
//     headers["token"] = `${token}`;
//   }

//   // console.log(process.env.REACT_APP_NAC_SVC);

//   const instance = axios.create({
//     // baseURL: '/api',
//     baseUrl: process.env.REACT_APP_NAC_SVC,
//     data: [],
//     errorHandle: false,
//     headers
//   });

//   instance.interceptors.response.use(
//     function(response) {
//       return response.data;
//     },
//     function(error) {
//       return Promise.reject(error);
//     }
//   );

//   return instance;
// }
