import React, { Component } from 'react';
import './App.css';

import { Router, Switch, Route } from 'react-router-dom';
import { PrivateRoute } from './PrivateRoute';

import routes from './AppRoutes';

const createBrowserHistory = require('history').createBrowserHistory;
export const history = createBrowserHistory();

export default class App extends Component {
  render() {
    const mainLayout = (
      <>
        {routes.map((route) => {
          // const exact = route.id !== 'dashboard';
          if(route.id === 'dashboard') {
            return (
            <PrivateRoute
              key={route.path}
              path={route.path}
              component={route.component}
            />)
          } else {
            return (<Route exact key={route.path} path={route.path} component={route.component} />)
          }
        })}
      </>
    )

    return (
      <Router history={history}>
        <Switch>
          {/* <Route exact path="/" name="Login" component={Login} /> */}
          {mainLayout}
        </Switch>
      </Router>
    );
  }
}
